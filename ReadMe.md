# My Custom Web Framework

I've tried frameworks like codeignitor, laravel and Symfony 2. Those were great but I was never in control and was never aware of what was going on under the hood (which in some way is a good thing). So while trying to learn how frameworks
works under the hood, I came across one good tutorial by [Patrick Louys](https://github.com/PatrickLouys/no-framework-tutorial). Taking it as a reference, I have built this custom framework for me which will work as a starting point for my future projects.


**Templating**
Each page is a twig template rendered by the server with react components used
where needed, most of the time as a root component.

**Nginx Note**
Need to change in the config file in /etc/nginx/sites-available/ location / and 
remove $uri/=404 and add index.php to route all the URI to first Controller.

### Backend
* *Language*: Hack/hhvm
* *Routing*: nikic/fast-route
* *Http-handler*: symfony/http-foundation
* *Templating engine*: twig
* *Dependency Injector*: rdlowrey/auryn
* *Error Handler (Dev)*: filp/whoops
* *Autoload*: psr-4 (namespace)

### Frontend
* *Template*: react (optional) (html rendered by server)
* *Dev Environment Tools*
    * *Bundler*: Webpack (decided to ditch wepack-dev-server and hmr as this is not a standalone front-end development)
    * *CSS pre-processor*: SASS
    * *JS Compilor*: Babel
    * *JS Linter*: eslint
    * *JS libraries*: jQuery, underscore
    * *Package Manager*: npm
    * *Task runner*: npm script

### Installation
* Install Composer - dependency manager for PHP [https://getcomposer.org/]
* Install NodeJs for npm - [https://docs.npmjs.com/getting-started/installing-node]
* Download or clone the project.
* From the root directory (where composer.josn file is located) run Composer install
* From the app directory (where package.json file is located) run npm install

## To dos
There are lot of things to do to improve the framework and some which are
* Make it secure
* Database integration
* Internationalization
* many more ...
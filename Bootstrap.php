<?hh
namespace MyFramework;

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/vendor/twig/twig/lib/Twig/Autoloader.php';

error_reporting(E_ALL);

$environment = 'development';

/**
 * Register the error handler
 */
$whoops = new \Whoops\Run();
if ($environment !== 'production') {
  $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
} else {
  $whoops->pushHandler(
    function($e) {
      echo 'Todo: Friendly error page and send an email to the developer';
    },
  );
}
$whoops->register();

/**
 * Symfony Request and Response injection
 */
$injector = include('src/config/Dependencies.php');
$http = $injector->make('MyFramework\Classes\Http\HttpWrapper');
$request = $http->request;
$response = $http->response;

include ('src/config/Routes.php');

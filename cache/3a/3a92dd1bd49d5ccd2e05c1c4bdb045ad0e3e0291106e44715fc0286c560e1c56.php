<?php

/* dashboard.html */
class __TwigTemplate_e4a481ff94b4a6c22730f0cb9fecd28e77190646f9e71116789344f17df1fc90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "dashboard.html", 1)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
        // line 2
        echo "<div id=\"reactMain\">

</div>
";
        // line 5
        $this->loadTemplate("footer.html", "dashboard.html", 5)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
    }

    public function getTemplateName()
    {
        return "dashboard.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 5,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' with pageData %}
<div id=\"reactMain\">

</div>
{% include 'footer.html' with pageData %}
", "dashboard.html", "/home/ram/Learn/myframework/src/templates/dashboard.html");
    }
}

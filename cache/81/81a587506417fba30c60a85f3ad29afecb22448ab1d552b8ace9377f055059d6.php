<?php

/* header.html */
class __TwigTemplate_7f7bdd771564a7e82753875dd531e161a289570f3232f8e3e62a92ff9ee8dc17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <title>";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pageData"]) ? $context["pageData"] : null), "pageTitle", array()), "html", null, true);
        echo "</title>
    <link rel=\"stylesheet\" href=\"css/style.css\" />
    ";
        // line 6
        if (($this->getAttribute((isset($context["pageData"]) ? $context["pageData"] : null), "pageTitle", array()) == "Login")) {
            // line 7
            echo "      <link rel=\"stylesheet\" href=\"css/login.css\"  />
    ";
        }
        // line 9
        echo "  </head>
    <body>
";
    }

    public function getTemplateName()
    {
        return "header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 9,  31 => 7,  29 => 6,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
  <head>
    <title>{{ pageData.pageTitle }}</title>
    <link rel=\"stylesheet\" href=\"css/style.css\" />
    {% if pageData.pageTitle == \"Login\" %}
      <link rel=\"stylesheet\" href=\"css/login.css\"  />
    {% endif %}
  </head>
    <body>
", "header.html", "/home/ram/Learn/myframework/src/templates/header.html");
    }
}

<?php

/* login.html */
class __TwigTemplate_0a92158d71bdf257f972e0a4d1cbc03a3456ffbb195f3c0d2ae2627b8fcc7e36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "login.html", 1)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
        // line 2
        echo "<div class=\"bordered-box\" id=\"login-box\">
  <div class=\"section-title\">
      System Authentication
  </div>
  <div class=\"box-container\">
    <form>
      <div class=\"form-row\">
        <label>Email</label>
        <input type=\"text\" name=\"email\"/>
      </div>
      <div class=\"form-row\">
        <label>Password</label>
        <input type=\"password\" />
      </div>
      <div id=\"action-group\">
        <button class=\"btn-enabled\" id=\"LoginButton\">Login</button>
        <a href=\"/dashboard\">Forgot Password</a>
      </div>
    </form>
  </div>
</div>
<script src=\"scripts/authenticate.bundle.js\"></script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' with pageData %}
<div class=\"bordered-box\" id=\"login-box\">
  <div class=\"section-title\">
      System Authentication
  </div>
  <div class=\"box-container\">
    <form>
      <div class=\"form-row\">
        <label>Email</label>
        <input type=\"text\" name=\"email\"/>
      </div>
      <div class=\"form-row\">
        <label>Password</label>
        <input type=\"password\" />
      </div>
      <div id=\"action-group\">
        <button class=\"btn-enabled\" id=\"LoginButton\">Login</button>
        <a href=\"/dashboard\">Forgot Password</a>
      </div>
    </form>
  </div>
</div>
<script src=\"scripts/authenticate.bundle.js\"></script>
</body>
</html>
", "login.html", "/home/ram/Learn/myframework/src/templates/login.html");
    }
}

<?php

/* secondPage.html */
class __TwigTemplate_ee7f229b14bb70decc905f1e5d616cd4ca8a4306a508653598b1d75b6473e2ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "secondPage.html", 1)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
        // line 2
        echo "<div id=\"secondPage\">
  <h1> Second Page</h1>
  <p>
    Welcome to MyFramework ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "
  </p>
</div>
";
        // line 8
        $this->loadTemplate("footer.html", "secondPage.html", 8)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
    }

    public function getTemplateName()
    {
        return "secondPage.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 8,  26 => 5,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' with pageData %}
<div id=\"secondPage\">
  <h1> Second Page</h1>
  <p>
    Welcome to MyFramework {{ name }}
  </p>
</div>
{% include 'footer.html' with pageData %}
", "secondPage.html", "/home/ram/Learn/myframework/src/templates/secondPage.html");
    }
}

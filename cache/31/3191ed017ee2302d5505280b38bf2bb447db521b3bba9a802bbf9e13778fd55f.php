<?php

/* demo.html */
class __TwigTemplate_8468d5e265d75d2e7bede2b46c39cc8ce7d1626e27614e47fd6c752087ac8702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\" />
    <title>Admin Template</title>
    <link rel=\"stylesheet\" href=\"css/style.css\" />
    <link rel=\"stylesheet\" href=\"css/demo.css\" />
    <script src=\"https://use.fontawesome.com/df90975a10.js\"></script>
  </head>
  <body>
    <div id=\"topBar\">
        Templates for Common UI to be used for my admin panels<sub>-Ram Kumar Bhandari</sub>
    </div>
    <div class=\"container\">
      <div id=\"color-palette\" class=\"bordered-box\">
        <div class=\"section-title\">
            Major colors for the website.
        </div>
        <div id=\"color-container\" class=\"section-container\">
          <div class=\"primary\"><p>Primary Color<br /><span>#22264b</span></p></div>
          <div class=\"accent\"><p>Accent Color<br /><span>#b56969</span></p></div>
        </div>
      </div>
      <h1>Heading One</h1>
      <h2>Heading Two</h2>
      <h3>Heading Three</h3>
      <h4>Heading Four</h4>
      <div class=\"horizontal-rule-dark\"></div>
      <button class=\"btn-enabled\">Enabled</button>
      <button class=\"btn-disabled\">Disabled</button>
      <button class=\"btn-enabled\"><i class=\"fa fa-hand-peace-o\" aria-hidden=\"true\"></i>With Icon</button>
      <p class=\"para\">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed ex ac ipsum ultrices egestas. Vivamus elementum dolor id tempus finibus.
        Proin rhoncus odio mattis volutpat scelerisque. Nullam malesuada diam nisl, ut cursus nulla porttitor ac. Suspendisse neque orci, blandit non ligula sed, tempus sagittis lectus.
        Aenean fermentum ut massa eget sollicitudin. Suspendisse sit amet nibh quis nunc dapibus dapibus faucibus eu dui. Donec ac tincidunt dolor.
        Sed blandit, lectus vel mattis posuere, velit nisl egestas eros, eget efficitur eros ipsum ut metus. Quisque id posuere libero. Mauris id nisi leo.
        Cras turpis leo, tristique nec aliquam quis, aliquam sed sapien. Integer rutrum blandit augue tristique scelerisque. Vivamus feugiat semper magna sed consequat.
        <br />
        Nullam tempor urna a porta commodo. Pellentesque consectetur lobortis leo vitae feugiat. Nunc rutrum imperdiet ex, eu pharetra sem rhoncus vitae.
        Vivamus malesuada turpis mattis mi cursus aliquet. Aenean consectetur arcu ac enim auctor, sed ullamcorper elit congue. Nullam lacinia tempus urna, lobortis egestas quam pharetra eget.
        Nulla blandit egestas elit eu vulputate. In arcu leo, tincidunt non interdum egestas, posuere et nisl. Duis hendrerit fringilla iaculis.
        Aenean feugiat ante nec erat fringilla scelerisque. Nulla facilisi.
      </p>
      <form>
        <div class=\"form-row\">
          <label>Short Label</label>
          <input type=\"text\" />
        </div>
        <div class=\"form-row\">
          <label>This is an example of long Label</label>
          <input type=\"text\" />
        </div>
        <div class=\"form-row\">
          <label>Checkbox</label>
          <div class=\"form-element-group-horizontal\">
            <input type=\"checkbox\" /> <span> Value One</span>
            <input type=\"checkbox\" /> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>Checkbox Vertical</label>
          <div class=\"form-element-group-vertical\">
            <input type=\"checkbox\" /> <span> Value One</span>
            <input type=\"checkbox\" /> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>Radio Button Vertical</label>
          <div class=\"form-element-group-vertical\">
            <input type=\"radio\" name=\"radioEx\"/> <span> Value One</span>
            <input type=\"radio\" name=\"radioEx\"/> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>This is an example of long Label</label>
            <textarea name=\"textarea\"></textarea>
        </div>
      </form>
      <div class=\"success-well\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
          Hey This is a success Well
      </div>
      <div class=\"warning-well\">
        <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>
          Hey This is a warning Well
      </div>
      <div class=\"error-well\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
          Hey This is an error Well
      </div>
      <div class=\"well\">
        This is just a well
      </div>
    </div>
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "demo.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\" />
    <title>Admin Template</title>
    <link rel=\"stylesheet\" href=\"css/style.css\" />
    <link rel=\"stylesheet\" href=\"css/demo.css\" />
    <script src=\"https://use.fontawesome.com/df90975a10.js\"></script>
  </head>
  <body>
    <div id=\"topBar\">
        Templates for Common UI to be used for my admin panels<sub>-Ram Kumar Bhandari</sub>
    </div>
    <div class=\"container\">
      <div id=\"color-palette\" class=\"bordered-box\">
        <div class=\"section-title\">
            Major colors for the website.
        </div>
        <div id=\"color-container\" class=\"section-container\">
          <div class=\"primary\"><p>Primary Color<br /><span>#22264b</span></p></div>
          <div class=\"accent\"><p>Accent Color<br /><span>#b56969</span></p></div>
        </div>
      </div>
      <h1>Heading One</h1>
      <h2>Heading Two</h2>
      <h3>Heading Three</h3>
      <h4>Heading Four</h4>
      <div class=\"horizontal-rule-dark\"></div>
      <button class=\"btn-enabled\">Enabled</button>
      <button class=\"btn-disabled\">Disabled</button>
      <button class=\"btn-enabled\"><i class=\"fa fa-hand-peace-o\" aria-hidden=\"true\"></i>With Icon</button>
      <p class=\"para\">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed ex ac ipsum ultrices egestas. Vivamus elementum dolor id tempus finibus.
        Proin rhoncus odio mattis volutpat scelerisque. Nullam malesuada diam nisl, ut cursus nulla porttitor ac. Suspendisse neque orci, blandit non ligula sed, tempus sagittis lectus.
        Aenean fermentum ut massa eget sollicitudin. Suspendisse sit amet nibh quis nunc dapibus dapibus faucibus eu dui. Donec ac tincidunt dolor.
        Sed blandit, lectus vel mattis posuere, velit nisl egestas eros, eget efficitur eros ipsum ut metus. Quisque id posuere libero. Mauris id nisi leo.
        Cras turpis leo, tristique nec aliquam quis, aliquam sed sapien. Integer rutrum blandit augue tristique scelerisque. Vivamus feugiat semper magna sed consequat.
        <br />
        Nullam tempor urna a porta commodo. Pellentesque consectetur lobortis leo vitae feugiat. Nunc rutrum imperdiet ex, eu pharetra sem rhoncus vitae.
        Vivamus malesuada turpis mattis mi cursus aliquet. Aenean consectetur arcu ac enim auctor, sed ullamcorper elit congue. Nullam lacinia tempus urna, lobortis egestas quam pharetra eget.
        Nulla blandit egestas elit eu vulputate. In arcu leo, tincidunt non interdum egestas, posuere et nisl. Duis hendrerit fringilla iaculis.
        Aenean feugiat ante nec erat fringilla scelerisque. Nulla facilisi.
      </p>
      <form>
        <div class=\"form-row\">
          <label>Short Label</label>
          <input type=\"text\" />
        </div>
        <div class=\"form-row\">
          <label>This is an example of long Label</label>
          <input type=\"text\" />
        </div>
        <div class=\"form-row\">
          <label>Checkbox</label>
          <div class=\"form-element-group-horizontal\">
            <input type=\"checkbox\" /> <span> Value One</span>
            <input type=\"checkbox\" /> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>Checkbox Vertical</label>
          <div class=\"form-element-group-vertical\">
            <input type=\"checkbox\" /> <span> Value One</span>
            <input type=\"checkbox\" /> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>Radio Button Vertical</label>
          <div class=\"form-element-group-vertical\">
            <input type=\"radio\" name=\"radioEx\"/> <span> Value One</span>
            <input type=\"radio\" name=\"radioEx\"/> <span> Value Two</span>
          </div>
        </div>
        <div class=\"form-row\">
          <label>This is an example of long Label</label>
            <textarea name=\"textarea\"></textarea>
        </div>
      </form>
      <div class=\"success-well\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
          Hey This is a success Well
      </div>
      <div class=\"warning-well\">
        <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>
          Hey This is a warning Well
      </div>
      <div class=\"error-well\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
          Hey This is an error Well
      </div>
      <div class=\"well\">
        This is just a well
      </div>
    </div>
  </body>
</html>
", "demo.html", "/home/ram/Learn/myframework/src/templates/demo.html");
    }
}

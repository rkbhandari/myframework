<?php

/* footer.html */
class __TwigTemplate_1271d81e0fb052e380b5ff4257dbc923da52d617af3e8629bc9cd0a8dabd6a7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "  <div>
    Copyright @ ";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pageData"]) ? $context["pageData"] : null), "copyright", array()), "html", null, true);
        echo "
  </div>
  <script src=\"scripts/bundle.js\"></script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("  <div>
    Copyright @ {{ pageData.copyright }}
  </div>
  <script src=\"scripts/bundle.js\"></script>
</body>
</html>
", "footer.html", "/home/ram/Learn/myframework/src/templates/footer.html");
    }
}

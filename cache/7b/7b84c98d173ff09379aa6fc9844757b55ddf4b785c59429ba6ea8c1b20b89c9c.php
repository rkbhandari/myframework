<?php

/* index.html */
class __TwigTemplate_ecbecd4fc9a880d90e517c135f1cd668bf0e75ffeeac0948425ea87f8adad982 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "index.html", 1)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
        // line 2
        echo "<div id=\"reactMain\">
  
</div>
";
        // line 5
        $this->loadTemplate("footer.html", "index.html", 5)->display(array_merge($context, (isset($context["pageData"]) ? $context["pageData"] : null)));
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 5,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' with pageData %}
<div id=\"reactMain\">
  
</div>
{% include 'footer.html' with pageData %}
", "index.html", "/home/ram/Learn/myframework/src/templates/index.html");
    }
}

import React from 'react';
import ReactDOM from 'react-dom';
import HomeController from './home/home-component-controller';

var element = document.getElementById('reactMain');
if (element !== undefined && element !== null && element !== "") {
  ReactDOM.render(<HomeController/>, document.getElementById('reactMain'));
}

import React from 'react';

class HomeController extends React.Component {
  constructor(props) {
    super(props);
    console.log();
  }
  render() {
      return(
        <div>
          <div>Hey This is from React</div>
          <a href="/second">Second Page</a>
        </div>
      );
  }
}

export default HomeController;

import $ from 'jquery';
import _ from 'underscore';

window.$ = window.jQuery = $;
window._ = _;

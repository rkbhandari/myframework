import Login from '../modules/login.js';
import $ from 'jquery';

$('#LoginButton').click((event) => {
  event.preventDefault();
  let login =  new Login();
  login.authenticate();
});

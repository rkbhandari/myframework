const webpack = require('webpack');
module.exports = {
  entry: {
    react: ['./global.js', './react/index.js'],
    module: [
      './global.js',
      './modules/login.js'
    ],
    authenticate: './javascripts/authenticate.js'
  },
  output: {
    path: '../public/scripts',
    filename: "[name].bundle.js"
  },
  watch: true,
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      mangle: false,
    })
  ],
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loaders: ['babel-loader']}
    ]
  }
};

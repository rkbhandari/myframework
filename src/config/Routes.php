<?hh
const CONTROLLER_PATH = 'MyFramework\Controller';
$dispatcher = \FastRoute\simpleDispatcher(
  function(\FastRoute\RouteCollector $r) {
    $r->addRoute(
      'GET',
      '/',
      [CONTROLLER_PATH.'\DefaultController', 'index'],
    );
    $r->addRoute(
      'GET',
      '/second',
      [CONTROLLER_PATH.'\DefaultController', 'secondPage'],
    );
    $r->addRoute(
      'GET',
      '/css-mock-up',
      [CONTROLLER_PATH.'\DefaultController', 'cssMockUp'],
    );
    $r->addRoute(
      'GET',
      '/dashboard',
      [CONTROLLER_PATH.'\DashboardController', 'index'],
    );
    $r->addRoute(
      'GET',
      '/another-route',
      function() {
        echo 'This works too';
      },
    );
    $r->addRoute(
      'POST',
      '/authenticate',
      [CONTROLLER_PATH.'\AuthenticateController', 'index'],
    );
  },
);

$routeInfo =
  $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
switch ($routeInfo[0]) {
  case \FastRoute\Dispatcher::NOT_FOUND:
    $response->setContent('404 - Page Not Found');
    $response->setStatusCode(
      \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    );
    $response->send();
    break;
  case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
    $response->setContent('405 - Method not allowed');
    $response->setStatusCode(405);
    break;
  case \FastRoute\Dispatcher::FOUND:
    if (is_array($routeInfo[1])) {
      $className = $routeInfo[1][0];
      $method = $routeInfo[1][1];
      $vars = $routeInfo[2];
      $class = $injector->make($className);
      $class->$method($vars);
    } else {
      $routeInfo[1]();
    }
    break;
}

<?hh

/** * Manage Dependency Injections */
$injector = new \Auryn\Injector();
$injector->share('MyFramework\Classes\Http\HttpWrapper');

/** Symfony Session */
$injector->share('Symfony\Component\HttpFoundation\Session\Session');
/***
 * Twig Template
 */
Twig_AutoLoader::register();
$loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
$params = ['cache' => "../cache", 'auto_reload' => true, 'debug' => true];
$injector->define('Twig_Environment', [$loader, $params]);
return $injector;

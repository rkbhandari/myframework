<?hh

namespace MyFramework\Controller;

use Twig_Environment;
use Symfony\Component\HttpFoundation\Session\Session;

use MyFramework\Classes\Http\HttpWrapper;
use MyFramework\Controller\Interfaces\PageController;
use MyFramework\Controller\Interfaces\RequiresAuthentication;

class DashboardController extends Controller
  implements PageController, RequiresAuthentication {

  public function __construct(
    HttpWrapper $http,
    Twig_Environment $renderer,
    Session $session,
  ) {
    parent::__construct($http, $renderer, $session);
  }

  public function index(): void {
    echo
      $this->renderPage(
        "dashboard.html",
        [
          'pageData' =>
            $this->buildPageData(['pageTitle' => 'Dashboard'])->toArray(),
        ],
      )
    ;
  }

  public function buildPageData(
    array<string, mixed> $data = [],
  ): ImmMap<arraykey, mixed> {
    $defaultData =
      ['pageTitle' => 'Home', 'copyright' => 'Ram Kumar Bhandari'];
    $data = array_merge($defaultData, $data);
    return new ImmMap($data);
  }
}

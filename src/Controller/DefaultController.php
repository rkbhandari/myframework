<?hh

namespace MyFramework\Controller;

use Twig_Environment;
use Symfony\Component\HttpFoundation\Session\Session;

use MyFramework\Classes\Http\HttpWrapper;
use MyFramework\Controller\Interfaces\PageController;

class DefaultController extends Controller implements PageController {

  public function __construct(
    HttpWrapper $http,
    Twig_Environment $renderer,
    Session $session,
  ) {
    parent::__construct($http, $renderer, $session);
  }

  public function index(): void {
    echo
      $this->renderPage(
        "login.html",
        [
          'pageData' =>
            $this->buildPageData(['pageTitle' => 'Login'])->toArray(),
        ],
      )
    ;
  }

  public function secondPage(): void {
    echo
      $this->renderPage(
        "secondPage.html",
        [
          'pageData' =>
            $this->buildPageData(['pageTitle' => 'Second Page'])
              ->toArray(),
          'name' => 'Ram',
        ],
      )
    ;
  }

  public function cssMockUp(): void {
    echo $this->renderPage("demo.html");
  }

  public function buildPageData(
    array<string, mixed> $data = [],
  ): ImmMap<arraykey, mixed> {
    $defaultData =
      ['pageTitle' => 'Home', 'copyright' => 'Ram Kumar Bhandari'];
    $data = array_merge($defaultData, $data);
    return new ImmMap($data);
  }
}

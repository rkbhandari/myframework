<?hh

namespace MyFramework\Controller;

use Symfony\Component\HttpFoundation\Session\Session;

use MyFramework\Classes\Http\HttpWrapper;

class AuthenticateController extends Controller {
  public function __construct(HttpWrapper $http, Session $session) {
    parent::__construct($http, null, $session);
  }
  public function index() {
    $params = $this->http->request->getRealMethod();
    die($params);
    $this->sendJSON([$params]);
  }
}

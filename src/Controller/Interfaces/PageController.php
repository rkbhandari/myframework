<?hh // strict

namespace MyFramework\Controller\Interfaces;

interface PageController {
  public function buildPageData(
    array<string, mixed> $data = [],
  ): ImmMap<arraykey, mixed>;
}

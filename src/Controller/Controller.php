<?hh

namespace MyFramework\Controller;

use Twig_Environment;
use Symfony\Component\HttpFoundation\Session\Session;

use MyFramework\Classes\Http\HttpWrapper;
use MyFramework\Classes\Application\ApplicationConstants;
use MyFramework\Controller\Interfaces\RequiresAuthentication;

class Controller {
  protected HttpWrapper $http;
  protected ?Twig_Environment $renderer;
  protected ?Session $session;

  const string LOGIN_PATH = '/';

  public function __construct(
    HttpWrapper $http,
    ?Twig_Environment $renderer = null,
    ?Session $session = null,
  ) {
    $this->http = $http;
    $this->renderer = $renderer;
    $this->session = $session;
    $this->checkIfThisControllerRequiresAuthentication();
  }

  protected function sendResponse(string $content): void {
    $this->http->response->setContent($content);
    $this->http->response->headers->set('Content-Type', 'text/plain');
    $this->http->response->setStatusCode(Response::HTTP_OK);
    $this->http->response->send();
  }

  protected function sendJSON(array $data, string $status = 'success'): void {
    $responseData = ['status' => $status, 'data' => $data];
    $this->http->response->setContent(json_encode($responseData));
    $this->http->response->headers->set('Content-Type', 'application/json');
    $this->http->response->send();
  }

  protected function renderPage(string $page, array $data = []): void {
    echo $this->renderer?->render($page, $data);
  }

  private function checkIfThisControllerRequiresAuthentication() {
    $reflectionClass = new \ReflectionClass($this);
    if ($reflectionClass->implementsInterface(
          'MyFramework\Controller\Interfaces\RequiresAuthentication',
        )) {
      if ($this->session?->get(ApplicationConstants::AUTHENTICATION_STATUS) ===
          null) {
        $redirect = $this->http->getRedirect(self::LOGIN_PATH);
        $redirect->send();
      }
    }
  }
}

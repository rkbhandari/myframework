<?hh

namespace MyFramework\Classes\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class HttpWrapper {
  /* HTTP status codes */
  const NOT_AUTHENTICATED = 401;
  const NOT_AUTHORISED = 403;

  public Request $request;
  public Response $response;

  public function __construct() {
    $this->request = Request::createFromGlobals();
    $this->response = new Response();
  }

  public function setRequest() {}
  public function setResponse() {}

  public function getRedirect(
    string $url,
    int $status = 302,
    array $headers = [],
  ): RedirectResponse {
    return new RedirectResponse($url, $status, $headers);
  }
}

<?hh

namespace MyFramework\Classes\Template\Interface;

interface Renderer {
  public function render();
}

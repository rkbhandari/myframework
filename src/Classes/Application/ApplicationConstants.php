<?hh

namespace MyFramework\Classes\Application;

class ApplicationConstants {

  /**
   * string to store authentication status in session
   */
  const string AUTHENTICATION_STATUS = 'LOGIN_STATUS';
}
